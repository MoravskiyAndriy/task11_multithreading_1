package com.moravskiyandriy.pipeconnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class ThreadsPipeConnection {
    private static final Logger logger = LogManager.getLogger(ThreadsPipeConnection.class);

    public void show() throws IOException {

        final PipedOutputStream output = new PipedOutputStream();
        PipedInputStream input = new PipedInputStream(output);

        Thread thread1 = getThread1(output);


        Thread thread2 = getThread2(input);

        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            logger.info("Interrupted exception found:");
            e.printStackTrace();
        }
    }

    private Thread getThread1(PipedOutputStream output) {
        return new Thread(() -> {
            try {
                for (int i = 0; i < 5; i++) {
                    output.write("\nSupplying information from input... ".getBytes());
                    Thread.sleep(2000);
                }
            } catch (IOException e) {
                logger.info("IO exception found:");
                e.printStackTrace();
            } catch (InterruptedException e) {
                logger.info("Interrupted exception found:");
                e.printStackTrace();
            }
        });
    }

    private Thread getThread2(PipedInputStream input) {
        return new Thread(() -> {
            try {
                int data = input.read();
                while (data != -1) {
                    System.out.print((char) data);
                    data = input.read();
                }
            } catch (IOException e) {
                logger.info("\nPipe was broken, no additional information will be sent.");
            }
        });
    }
}
