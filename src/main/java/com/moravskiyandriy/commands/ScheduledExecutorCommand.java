package com.moravskiyandriy.commands;

import com.moravskiyandriy.scheduledexecutor.ScheduledExecutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class ScheduledExecutorCommand implements Command {
    private static final Logger logger = LogManager.getLogger(ScheduledExecutorCommand.class);

    @Override
    public void execute() {
        Scanner input = new Scanner(System.in);
        logger.info("Input number: ");
        int number = input.nextInt();
        new ScheduledExecutor(number).show();
    }
}
