package com.moravskiyandriy.commands;

import java.util.ArrayList;
import java.util.List;

public class Menu {
    private CommandSwitcher commandSwitcher;
    private List<String> menu = new ArrayList<>();

    public Menu() {
        commandSwitcher = new CommandSwitcher();
        commandSwitcher.addCommand(new PingPongCommand());
        menu.add("0. PingPong");
        commandSwitcher.addCommand(new FibonacciCommand());
        menu.add("1. FibonacciRunnable numbers");
        commandSwitcher.addCommand(new FibonacciExecutorsCommand());
        menu.add("2. FibonacciRunnable numbers with executors");
        commandSwitcher.addCommand(new ScheduledExecutorCommand());
        menu.add("3. Sleepy task.");
        commandSwitcher.addCommand(new CriticalSectionCommand());
        menu.add("4. Critical section task.");
        commandSwitcher.addCommand(new PipeConnectionCommand());
        menu.add("5. Pipe connection task.");
        menu.add("Type 'q' to quit.");
    }

    public void chose(final Integer number) {
        commandSwitcher.execute(number);
    }

    public void printMenu() {
        for (String s : menu) {
            System.out.println(s);
        }
    }
}
