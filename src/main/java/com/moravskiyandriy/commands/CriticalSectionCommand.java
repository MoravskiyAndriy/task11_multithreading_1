package com.moravskiyandriy.commands;

import com.moravskiyandriy.criticalsection.CriticalSectionOneResource;
import com.moravskiyandriy.criticalsection.CriticalSectionThreeResources;

public class CriticalSectionCommand implements Command {
    @Override
    public void execute() {
        new CriticalSectionOneResource().show();
        new CriticalSectionThreeResources().show();
    }
}
