package com.moravskiyandriy.commands;

import com.moravskiyandriy.fibonacci.FibonacciApp;

public class FibonacciCommand implements Command {
    @Override
    public void execute() {
        new FibonacciApp().show();
    }
}
