package com.moravskiyandriy.commands;

import com.moravskiyandriy.pingpong.PingPong;

public class PingPongCommand implements Command {
    public void execute() {
        new PingPong().show();
    }
}
