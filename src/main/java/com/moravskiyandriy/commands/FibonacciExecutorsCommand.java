package com.moravskiyandriy.commands;

import com.moravskiyandriy.fibonacci.ExecutorsApp;

public class FibonacciExecutorsCommand implements Command {
    @Override
    public void execute() {
        new ExecutorsApp().show();
    }
}
