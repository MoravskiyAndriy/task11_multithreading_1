package com.moravskiyandriy.commands;

import com.moravskiyandriy.pipeconnection.ThreadsPipeConnection;

import java.io.IOException;

public class PipeConnectionCommand implements Command {
    @Override
    public void execute() {
        try {
            new ThreadsPipeConnection().show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
