package com.moravskiyandriy;

import com.moravskiyandriy.commands.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Application {
    private static final Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Menu menu = new Menu();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            logger.info("\nMenu: ");
            menu.printMenu();
            logger.info("Your choice:");
            String choice = scanner.nextLine();
            if (choice.equals("q")) {
                break;
            }
            menu.chose(Integer.valueOf(choice));
        }
    }
}
