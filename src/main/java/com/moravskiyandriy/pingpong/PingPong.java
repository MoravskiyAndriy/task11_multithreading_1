package com.moravskiyandriy.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static final Logger logger = LogManager.getLogger(PingPong.class);
    private static final Object sync = new Object();

    public void show() {
        Thread t1 = getThread1();
        Thread t2 = getThread2();

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            logger.info("InterruptedException:");
            e.printStackTrace();
        }
    }

    private Thread getThread1() {
        return new Thread(() -> {
            synchronized (sync) {
                for (int i = 1; i <= 50; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        logger.info("InterruptedException:");
                        e.printStackTrace();
                    }
                    logger.info("Ping!");
                    sync.notify();
                }
                System.out.println("finish " + Thread.currentThread().getName());
            }
        });
    }

    private Thread getThread2() {
        return new Thread(() -> {
            synchronized (sync) {
                for (int i = 1; i <= 50; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        logger.info("InterruptedException:");
                        e.printStackTrace();
                    }
                    logger.info("Pong!");
                }
                System.out.println("finish " + Thread.currentThread().getName());
            }
        });
    }
}
