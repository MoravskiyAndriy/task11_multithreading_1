package com.moravskiyandriy.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Integer> {
    private static final Logger logger = LogManager.getLogger(FibonacciRunnable.class);
    private int quantity;
    private String name;

    FibonacciCallable(final int quantity, final String name) {
        this.quantity = quantity;
        this.name = name;
    }

    @Override
    public Integer call() throws Exception {
        int f1 = 1;
        int f2 = 0;
        ArrayList<Integer> fibonacciNumbers = new ArrayList<>();
        fibonacciNumbers.add(1);
        fibonacciNumbers.add(1);
        for (int i = 2; i < quantity; i++) {
            fibonacciNumbers.add(fibonacciNumbers.get(i - 1) + fibonacciNumbers.get(i - 2));
        }
        return fibonacciNumbers.stream().mapToInt(m -> m).sum();
    }
}
