package com.moravskiyandriy.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.*;

public class ExecutorsApp {
    private static final Logger logger = LogManager.getLogger(ExecutorsApp.class);

    public void show() {
        logger.info("Executor: ");

        FibonacciRunnable fR1 = new FibonacciRunnable(5, "f1");
        FibonacciRunnable fR2 = new FibonacciRunnable(9, "f2");
        FibonacciRunnable fR3 = new FibonacciRunnable(7, "f3");

        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(fR1);
        executor.execute(fR2);
        executor.execute(fR3);

        logger.info("ExecutorService: ");

        FibonacciCallable fC1 = new FibonacciCallable(5, "f1");
        FibonacciCallable fC2 = new FibonacciCallable(9, "f2");
        FibonacciCallable fC3 = new FibonacciCallable(7, "f3");

        ExecutorService executor1 = Executors.newSingleThreadExecutor();

        Future<Integer> f1Result = executor1.submit(fC1);
        Future<Integer> f2Result = executor1.submit(fC2);
        Future<Integer> f3Result = executor1.submit(fC3);

        try {
            logger.info("f1Result" + f1Result.get());
            logger.info("f2Result" + f2Result.get());
            logger.info("f3Result" + f3Result.get());
        } catch (InterruptedException e) {
            logger.info("InterruptedException:");
            e.printStackTrace();
        } catch (ExecutionException e) {
            logger.info("ExecutionException:");
            e.printStackTrace();
        }

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        executorService.scheduleAtFixedRate(fR2, 1, 2, TimeUnit.SECONDS);

        try {
            executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            logger.info("InterruptedException:");
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}
