package com.moravskiyandriy.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class FibonacciRunnable implements Runnable {
    private static final Logger logger = LogManager.getLogger(FibonacciRunnable.class);
    private int quantity;
    private String name;

    FibonacciRunnable(final int quantity, final String name) {
        this.quantity = quantity;
        this.name = name;
    }

    @Override
    public void run() {
        int f1 = 1;
        int f2 = 0;
        ArrayList<Integer> fibonacciNumbers = new ArrayList<>();
        fibonacciNumbers.add(1);
        fibonacciNumbers.add(1);
        for (int i = 2; i < quantity; i++) {
            fibonacciNumbers.add(fibonacciNumbers.get(i - 1) + fibonacciNumbers.get(i - 2));
        }
        fibonacciNumbers.forEach(n -> logger.info(name + ": " + n));
    }
}
