package com.moravskiyandriy.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciApp {
    private static final Logger logger = LogManager.getLogger(FibonacciApp.class);

    public void show() {
        FibonacciRunnable f1 = new FibonacciRunnable(5, "f1");
        FibonacciRunnable f2 = new FibonacciRunnable(9, "f2");
        FibonacciRunnable f3 = new FibonacciRunnable(7, "f3");

        Thread t1 = new Thread(f1);
        Thread t2 = new Thread(f2);
        Thread t3 = new Thread(f3);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            logger.info("InterruptedException:");
            e.printStackTrace();
        }
    }
}
