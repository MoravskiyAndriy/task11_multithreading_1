package com.moravskiyandriy.scheduledexecutor;

import java.util.concurrent.*;

public class ScheduledExecutor {
    private int quantity;

    public ScheduledExecutor(int quantity) {
        this.quantity = quantity;
    }

    public void show() {
        ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(1);
        scheduledPool.scheduleAtFixedRate(new SleepyThread(), 0, 10, TimeUnit.SECONDS);

        try {
            TimeUnit.MILLISECONDS.sleep((int) ((quantity) * 10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        scheduledPool.shutdown();
    }
}
