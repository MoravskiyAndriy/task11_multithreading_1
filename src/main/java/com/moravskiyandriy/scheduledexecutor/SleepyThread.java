package com.moravskiyandriy.scheduledexecutor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

public class SleepyThread implements Runnable {
    private static final Logger logger = LogManager.getLogger(SleepyThread.class);

    @Override
    public void run() {
        Random rand = new Random();
        int time = rand.nextInt(10);
        try {
            Thread.sleep(time * 1000);
            logger.info("was asleep for " + time + " seconds.");
        } catch (InterruptedException e) {
            logger.info("InterruptedException:");
            e.printStackTrace();
        }
    }
}
