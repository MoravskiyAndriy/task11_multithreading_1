package com.moravskiyandriy.criticalsection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CriticalSectionOneResource {
    private static final Logger logger = LogManager.getLogger(CriticalSectionOneResource.class);
    private final Object RESOURCE = new Object();

    public void show() {
        logger.info("\nOne resource:\n");
        Thread t1 = new Thread(this::method1);
        Thread t2 = new Thread(this::method2);
        Thread t3 = new Thread(this::method3);

        t1.start();
        t2.start();
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (Exception e) {
            System.out.println("Interrupted");
        }
    }

    private void method1() {
        synchronized (RESOURCE) {
            for (int i = 1; i <= 10; i++) {
                logger.info("method1 is working...");
            }
        }
    }

    private void method2() {
        synchronized (RESOURCE) {
            for (int i = 1; i <= 10; i++) {
                logger.info("method2 is working...");
            }
        }
    }

    private void method3() {
        synchronized (RESOURCE) {
            for (int i = 1; i <= 10; i++) {
                logger.info("method3 is working...");
            }
        }
    }
}
